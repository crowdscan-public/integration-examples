# MIT License
#
# Copyright (c) 2023 CrowdScan
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import asyncio
import aiohttp
import csv
import logging
from decouple import config

logger = logging.getLogger(__name__)
formatstring = "%(relativeCreated)d - %(name)s - %(levelname)s - %(message)s"
logging.basicConfig(format=formatstring, level=logging.INFO)
csv_header = ['time', 'env', 'zone', 'count']

class CrowdScan_Api_receiver():
    def __init__(self, _base_url=None, _timedelta=5, _api_timeout=10, _filename="output.csv", _token=""):
        self._base_url = _base_url
        self._timedelta = _timedelta
        self._api_timeout = _api_timeout
        self._url = _base_url
        self._filename = _filename
        self._token = _token
        self.writer = None
        if config("API_STORE_CSV") == "true":
            self.write_header()

    def write_header(self):
        logger.info('write header')
        with open(self._filename, 'w+', encoding='utf-8') as f:
            self.writer = csv.writer(f)
            self.writer.writerow(csv_header)
            f.close()

    async def do_request(self):
        headers = {"Authorization": "Bearer " + self._token}
        async with aiohttp.ClientSession(headers=headers) as session:
            async with session.get(self._url) as resp:
                # yield control for 10 second
                await asyncio.sleep(self._api_timeout)
                data = await resp.json()
                logger.info("response from {} is {}".format(self._url, data))
                if config("API_STORE_CSV") == "true":
                    with open(self._filename, 'a') as f:
                        self.writer = csv.DictWriter(f, fieldnames=csv_header)
                        timestamp = data["header"]["time"]
                        env = data["header"]["environment"]
                        for zone_index, zone in enumerate(data["payload"]["regions"]):
                            csvdata = {"time": timestamp, "env": env, "zone": zone_index, "count": zone}
                            self.writer.writerow(csvdata)
                            logger.info("data of is {}".format(csvdata))
                else:
                    if resp.status == 201:
                        regiondata = data["payload"]["regions"]
                        logger.info("data of {} is {}".format(data["header"]["time"], regiondata))

if __name__ == "__main__":
    logger.info("start reader")
    api_receiver = CrowdScan_Api_receiver(_base_url=config("API_URL"),
                                          _timedelta=config("API_TIMEDELTA"),
                                          _api_timeout=config("API_TIMEOUT", cast=int),
                                          _filename=config("API_STORE_FILENAME"),
                                          _token=config("API_TOKEN"))
    while True:
        try:
            asyncio.run(api_receiver.do_request())
        except KeyboardInterrupt:
            logger.info("received KeyboardInterrupt... stopping processing")