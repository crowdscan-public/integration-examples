# MIT License
#
# Copyright (c) 2023 CrowdScan
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import logging, time
import paho.mqtt.client as mqtt
from Data import dataclasses
from decouple import config

logging.basicConfig(level=logging.DEBUG)
mqttc = mqtt.Client()
logger = logging.getLogger(__name__)
mqttc.enable_logger(logger)

def on_message(mosq, obj, msg):
    _msg = dataclasses.CrowdMsg.from_json(msg.payload.decode())
    logger.info("topic is {} and msg is {}".format(msg.topic, _msg))

if __name__ == "__main__":
    mqttc.on_message = on_message
    mqttc.username_pw_set(username=config("BROKER_USER"), password=config("BROKER_PASSWORD"))
    mqttc.connect(config("BROKER_URI"), 1883, 60)
    mqttc.subscribe(config("BROKER_TOPIC"))
    mqttc.loop_forever()