# MIT License
#
# Copyright (c) 2023 CrowdScan
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import asyncio
import aiohttp
import logging, sys, datetime
from influxdb import InfluxDBClient
from decouple import config

logger = logging.getLogger(__name__)
formatstring = "%(relativeCreated)d - %(name)s - %(levelname)s - %(message)s"
logging.basicConfig(format=formatstring, level=logging.INFO)


def connect_influx(uri, db_name, username=None, password=None):
    logger.info("conncect to influx")
    try:
        influx_client = InfluxDBClient(host=uri, port=8086, username=username,
                                       password=password, database=db_name)
        try:
            logger.info("create database {}".format(db_name))
            influx_client.create_database(db_name)
        except Exception as e:
            logger.error("{}".format(e))
        return influx_client
    except:
        return None

def save_to_influx(influx_client, data, env, region_id, time):
    time_replaced_timezone = time.replace('Z', '+00:00')
    time_str = datetime.datetime.fromisoformat(time_replaced_timezone)
    time_str = time_str.isoformat()
    logger.debug("store env: {} data in Influxdb".format(str(env)))
    json_body = [
        {
            "measurement": env,
            "tags": {
                "region": str(region_id)
            },
            "fields": data,
            "time": time_str
        }
    ]
    try:
        influx_client.write_points(json_body)
    except Exception as err:
        logger.warning("Unexpected error: {}".format(str(err)))

class CrowdScan_Api_receiver():
    def __init__(self, _base_url=None, _timedelta=5, _api_timeout=10, _influx=None):
        self._base_url = _base_url
        self._timedelta = _timedelta
        self._api_timeout = _api_timeout
        self._url = self.create_custom_url()
        self._influx = _influx

    def create_custom_url(self):
        return self._base_url + "/count"

    async def do_request(self):
        async with aiohttp.ClientSession() as session:
            async with session.get(self._url) as resp:
                # yield control for 10 second
                await asyncio.sleep(self._api_timeout)
                data = await resp.json()
                logger.info("response from {} is {}".format(self._url, data))
                regiondata = data["payload"]["regions"]
                logger.info("data of {} is {}".format(data["header"]["time"], regiondata))
                influxData = dict()
                for index, value in enumerate(regiondata):
                    influxData["crowd_count"] = value
                    save_to_influx(self._influx, influxData, data["header"]["environment"], index, data["header"]["time"])
                logger.info("data got stored in influx")

if __name__ == "__main__":
    logger.info("start reader")
    api_receiver = None
    influx = connect_influx(config("INFLUX_URL"), config("INFLUX_DATABASE"), config("INFLUX_USERNAME"), config("INFLUX_PASSWORD"))
    if influx is not None:
        api_receiver = CrowdScan_Api_receiver(_base_url=config("API_URL"),
                                              _timedelta=config("API_TIMEDELTA"),
                                              _api_timeout=config("API_TIMEOUT", cast=int),
                                              _influx=influx)
        while True:
            try:
                asyncio.run(api_receiver.do_request())
            except KeyboardInterrupt:
                logger.info("received KeyboardInterrupt... stopping processing")
    else:
        logger.error("Could not connect to Database")
