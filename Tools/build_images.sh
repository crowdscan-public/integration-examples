#!/bin/bash
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -t|--tag) tag="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

echo Start building all stack application for tag $tag
DIR=$(find ../Examples -type d -maxdepth 1)
FILE="/Dockerfile"
for FOLDER in ${DIR}
  do
    DOCKERFILE=$FOLDER$FILE
    echo ${DOCKERFILE}
    APPNAME=$(basename $FOLDER)
    APPNAME=$(echo "$APPNAME" | tr '[:upper:]' '[:lower:]')
    echo ${APPNAME}
    docker build -f ${DOCKERFILE} -t crowdscan/${APPNAME}:$tag ..
  done