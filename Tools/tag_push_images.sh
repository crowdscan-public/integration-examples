#!/bin/bash
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -t|--tag) tag="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

echo retagging all stack application to $tag and push them to gitlab
DIR=$(find ../Examples -type d -maxdepth 1)
FILE="/Dockerfile"
for FOLDER in ${DIR}
  do
    DOCKERFILE="$FOLDER$FILE"
    echo ${DOCKERFILE}
    APPNAME=$(basename $FOLDER)
    APPNAME=$(echo "$APPNAME" | tr '[:upper:]' '[:lower:]')
    echo ${APPNAME}
    docker tag crowdscan/${APPNAME}:$tag registry.gitlab.com/crowdscan-public/integration-examples/${APPNAME}:$tag
    docker image push registry.gitlab.com/crowdscan-public/integration-examples/${APPNAME}:$tag
    docker tag crowdscan/${APPNAME}:$tag registry.gitlab.com/crowdscan-public/integration-examples/${APPNAME}:latest
    docker image push registry.gitlab.com/crowdscan-public/integration-examples/${APPNAME}:latest
  done