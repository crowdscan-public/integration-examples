##################################################################
# Author: ben.bellekens@crowdscan.be
##################################################################
.PHONY: init deploy build

init:
	@echo -n init repository
	$(shell git submodule init && git submodule update)
	@echo -n install sdk python requirements
	$(shell cd Libs/sdk && sudo pip3 install -r requirements.txt)
	$exit 1

# tag variable is necessary
#ok - all applications are being build based on the occurence of a Dockerfile. You also need to specify a tag
# eg. " make tag=v1 build"
build:
	@echo -n Make Start building all stack application $(tag)
	$(shell cd Tools && ./build_images.sh -t $(tag))

kill:
	@echo "stop current docker-compose deployment"
	docker-compose -f docker-compose.stack.yml kill

#ok - clean all images related to a specific tag
#eg. " make tag=v1 clean "
clean:
	@echo remove local container images with tag $(tag)
	docker image ls | grep $(tag) | awk '{print $$1 ":" $$2}' | xargs docker rmi -f
	docker rm $$(docker ps -a -q)
