
# CrowdScan-public
A brief description of what this project does and who it's for

## Getting started with Docker and Docker compose

To get started, you can use our pre-build Docker images in combination with the provided Docker compose file. 
 - crowdscan/api_csv_client:latest

Make sure, you set the correct environment variables. The following variables in the .env file are required for the image api_csv_client.
- API_URL=<provided by CrowdScan>
- API_TIMEDELTA=15
- API_TIMEOUT=60
- API_STORE_CSV=True
- API_STORE_FILENAME="/app/data/output.csv"

```bash
   $docker-compose up
```

## Requirements
This projects requires some pre-installed packages in order to get alone:
- Docker desktop
- docker-compose
- Make
- Python3 (tested on Python 3.7)
- python-pip

## Installation
get username and password from CrowdScan mosquitto server and add it in local .env file. When your shell asks for a password,
this is necessary to install all pip requirements.

```bash
  $git clone https://gitlab.com/crowdscan-public/integration-examples.git
  $make init
```
The command _make init_ will clone all submodules and will install the specific python requirements via pip
## Usage/Examples
To get data from the servers, two examples were included:
1. API: This example will allow you to fetch asynchronously the data from the server via the REST API.
2. MQTT: This example will allow you to listen on the mosquitto broker of CrowdScan. For this example you will need to 
request the proper authentication and subscription topic in order to connect with the broker and receive the data. 

## Deployment

To deploy this project, you first need to build the docker images and afterwards you can deploy it.
The tag parameter enables you to specify a specific tag (eg. v1, test, beta).

```bash
  $make tag=test build
  $docker-compose up
```

What happens behind the scenes? 
- Executing the build_images.sh scrips, which will build a seperate docker image for each example.
- Secondly, it will run the "_docker-compose up_" for the specific tags. In the docker-compose file, a seperate volume is created
to store the requested data in a persistent way.

## Authors

- [@Ben Bellekens](mailto:ben.bellekens@crowdscan.be)
- [@Kaumudi Singh](mailto:kaumudi.singh@crowdscan.be)

